<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class BeerControllerTest extends WebTestCase
{
    public function testShowBeers()
    {
        $client = static::createClient();

        $client->request('GET', '/api/beers', ['food' => 'vanilla']);

        // asserts that the "Content-Type" header is "application/json"
        $this->assertTrue(
            $client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            ),
            'the "Content-Type" header is "application/json"' // optional message shown on failure
        );

        // asserts that the response content contains a string
        $this->assertStringContainsString(
            'Rhubarb Saison - B-Sides',
            $client->getResponse()->getContent()
        );

        // asserts that the response status code is 2xx
        $this->assertTrue($client->getResponse()->isSuccessful(), 'response status is 2xx');
        // asserts that the response status code is 404
        $this->assertTrue($client->getResponse()->isNotFound());
    }

    public function testShowBeerDetails()
    {
        $client = static::createClient();

        $client->request('GET', '/api/beers/10');

        // asserts that the "Content-Type" header is "application/json"
        $this->assertTrue(
            $client->getResponse()->headers->contains(
                'Content-Type',
                'application/json'
            ),
            'the "Content-Type" header is "application/json"' // optional message shown on failure
        );

        // asserts that the response content contains a string
        $this->assertStringContainsString(
            'Bramling X',
            $client->getResponse()->getContent()
        );
    }
}