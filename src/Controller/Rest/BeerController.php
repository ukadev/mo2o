<?php

namespace App\Controller\Rest;

use App\Service\BeerService;
use App\Service\BeerDetailsService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class BeerController extends AbstractFOSRestController
{

    /**
     * Get a list of beers filtered by Food parameter
     * @param Request $request
     * @return JsonResponse
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function getBeersAction(Request $request)
    {
        $beerService = new BeerService();
        $beers = $beerService->getList($request);

        return JsonResponse::fromJsonString($beers);
;    }

    /**
     * Get the details of one Beer, filtered by Id
     * @param int $id
     * @return JsonResponse
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function getBeerAction(int $id)
    {
        $beerDetailsService = new BeerDetailsService();

        return JsonResponse::fromJsonString($beerDetailsService->getDetails($id));
    }
}