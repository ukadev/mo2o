<?php

namespace App\Service;


use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerBuilder as Serializer;

class BeerDetailsService
{
    private $apiClient;

    public function __construct()
    {
        $this->apiClient = new PunkApiClient();
    }

    /**
     * @param int $id
     * @return string
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function getDetails(int $id): string
    {
        $serializer = Serializer::create()->build();
        $beer = $this->apiClient->getById($id);

        return $serializer->serialize($beer, 'json', SerializationContext::create()->setGroups(['details']));
    }
}