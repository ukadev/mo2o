<?php

namespace App\Service;

use App\Entity\Beer;
use App\Factory\BeerFactory;
use GuzzleHttp\Client;


class PunkApiClient
{
    /**
     * @var string
     */
    private $apiRoot = 'https://api.punkapi.com/v2/beers';
    /**
     * @var array
     */
    private $params = [];
    /**
     * @var array
     */
    private $allowedParams = [
        'food',
        'page',
    ];

    /**
     * PunkApiClient constructor.
     *
     */
    public function __construct()
    {
        $this->client = new Client;
    }

    /**
     * Returns the URL that would be hit at the current state of this object.
     *
     * @return string
     */
    public function getEndpoint()
    {
        return rtrim($this->apiRoot . '?' . http_build_query($this->params), '?');
    }

    /**
     * Queries the PunkAPI with the current parameters of this object.
     *
     * @return array of \StdClass objects
     * @throws \InvalidArgumentException (via GuzzleHttp\json_decode()
     */
    public function get()
    {
        $response = $this->client->get($this->getEndpoint());
        $list = \GuzzleHttp\json_decode($response->getBody(), true);

        return array_map(function ($array) {
            return BeerFactory::fromArray($array);
        }, $list);
    }

    /**
     * Get one result from the API by it's ID number
     *
     * @param $id
     * @return \App\Entity\Beer object
     */
    public function getById(int $id): Beer
    {
        $response = $this->client->get($this->apiRoot . '/' . $id);
        $beer = \GuzzleHttp\json_decode($response->getBody(), true)[0];

        return BeerFactory::fromArray($beer);
    }

    /**
     * Adds parameter options to this object.
     *
     * @param array $params
     * @return $this
     */
    public function addParams(Array $params)
    {
        $this->params = array_merge($this->params, $this->cleanParams($params));

        return $this;
    }

    /**
     * Set the parameters to return the given page of results
     *
     * @param $pageNumber
     * @return $this
     */
    public function page($pageNumber)
    {
        $this->addParams(['page' => $pageNumber]);

        return $this;
    }

    /**
     * Sets the food parameter to the given food name
     *
     * @param $foodName
     * @return $this
     */
    public function food($foodName)
    {
        $this->addParams(['food' => $foodName]);

        return $this;
    }


    /**
     * Helper method, parameter validation-ish.
     *
     * @param $params
     * @return array
     */
    private function cleanParams($params)
    {
        return array_filter(
            $params,
            function ($key) {
                return in_array($key, $this->allowedParams);
            },
            ARRAY_FILTER_USE_KEY
        );
    }
}