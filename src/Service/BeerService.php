<?php

namespace App\Service;


use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use JMS\Serializer\SerializerBuilder as Serializer;

class BeerService
{
    private $apiClient;

    public function __construct()
    {
        $this->apiClient = new PunkApiClient();
    }

    /**
     * @param Request $request
     * @return string
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function getList(Request $request): string
    {
        $food = $request->get('food', null);
        $page = (int)$request->get('page', 1);

        // check for "food" filter inside url parameters
        if (!$food) {
            Throw new BadRequestHttpException('Invalid value for \'food\' parameter');
        }

        // Add food filter to the api parameters array
        $this->apiClient->addParams(['food' => $food]);

        // if page filter is passed as url parameter and higher than 1, set as api param
        if ($page > 1) {
            $this->apiClient->addParams(['page' => $page]);
        }

        $beers = $this->apiClient->get();

        $serializer = Serializer::create()->build();

        return $serializer->serialize($beers, 'json', SerializationContext::create()->setGroups(['list']));
    }
}