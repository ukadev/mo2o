<?php

namespace App\Factory;

use App\Entity\Beer;

class BeerFactory
{
    public static function fromArray($array)
    {
        $beer = new Beer();

        $beer->setId($array['id'])
            ->setName($array['name'])
            ->setDescription($array['description'])
            ->setFirstBrewed($array['first_brewed'])
            ->setTagline($array['tagline'])
            ->setImageUrl($array['image_url']);

        return $beer;
    }
}