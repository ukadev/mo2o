## Installation

- Clone the repository

```
git clone git@github.com:ukadev/mo2o.git
```

- Execute composer

```
composer install && composer update
```


## Api access

This application has two different URL access points.

### Beer list

- Path: `/api/beers`. there is a mandatory parameter (food). As optional parameter, you can select the page (by default 1)
 
```
curl /api/beers?food=vanilla
curl /api/beers?food=vanilla&page=2
```


### Details

If you want to get the details of a beer, you have to access to `/api/beers/{id}` replacing `{id}` with the ID of the beer you want to retrieve the info.

```
curl /api/beers/12
```